package ru.kemsu.misc.legobot.service

import ru.kemsu.misc.MiscFluxApplication

import org.junit.jupiter.api.Test
import org.ocpsoft.prettytime.nlp.PrettyTimeParser
import org.springframework.boot.test.context.SpringBootTest
import ru.kemsu.misc.helper.println
import java.text.SimpleDateFormat
import java.util.*

@SpringBootTest(classes = [ru.kemsu.misc.MiscFluxApplication::class])
internal class PrettyParserTest {

    @Test
    fun prettyParser() {
        val testData = listOf(
            "В понедельник",
            "Во вторник",
            "В среду",
            "В четверг",
            "В пятницу",
            "В субботу",
            "В воскресенье",
            "Сегодня",
            "Завтра"
        )
        testData.forEach {
            val result = PrettyParser().parse(it,"9:00")
            "\"$it\" to \"$result\",".println()
        }
    }

    @Test
    fun parse() {
      //  val time = LocalTime.now().toString()
      //  PrettyParser().parse("17.01",time).println()
        PrettyTimeParser().parse("10.40").println()
    }

    @Test
    fun parseByPattern() {
        val dateAndTime = "01.02.2023 17:00"
        val simpleDateFormat = SimpleDateFormat("d.M.y HH:mm", Locale("ru", "RU"))
        simpleDateFormat.parse(dateAndTime).also { it.println() }
    }



}