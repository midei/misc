package ru.kemsu.misc

import org.springframework.boot.runApplication
class MiscFluxApplication

fun main(args: Array<String>) {
	runApplication<MiscFluxApplication>(*args)
}
