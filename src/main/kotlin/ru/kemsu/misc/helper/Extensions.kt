package ru.kemsu.misc.helper

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.api.client.googleapis.json.GoogleJsonResponseException

fun Any?.println(): Unit = println(this)
fun Any?.printError(): Unit = System.err.println(this)
val mapper = jacksonObjectMapper().also { it.registerModule(JavaTimeModule()) }
fun Any.toJson(): String = mapper.writeValueAsString(this)
inline fun <reified T> String.readJson(): T = mapper.readValue(this)
inline fun <reified T> ByteArray.readJson(): T = mapper.readValue(this)
fun Throwable.getGoogleApiMessage(): String? = if (this is GoogleJsonResponseException)  {
    if (this.details == null) this.statusMessage else this.details.message
}
else this.message.toString()