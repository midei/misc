package ru.kemsu.misc.service.security




import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import ru.kemsu.misc.helper.readJson
import ru.kemsu.misc.service.security.model.AuthData
import ru.kemsu.misc.service.security.model.AuthErrorResponse
import java.util.*

@Service
class AuthService(@Value(value = "\${security.url}") securityUrl: String) {

    private lateinit var webClient: WebClient

    init { webClient = WebClient.builder().baseUrl(securityUrl).build() }


    private val decoder = Base64.getDecoder()
    private val authTheme = "Bearer"


    fun authenticationWebFilter(): AuthenticationWebFilter {
        val webFilter = AuthenticationWebFilter(customersAuthenticationManager())
        webFilter.setServerAuthenticationConverter { exchange ->
            resolveToken(exchange)
                .filterWhen { token -> validateToken(token) }
                .map { token ->
                    val payload = token.split(".")[1]
                    val authData = decoder.decode(payload).readJson<AuthData>()
                    authData.token = token
                    UsernamePasswordAuthenticationToken(
                        authData.username,
                        authData,
                        authData.roles.map { SimpleGrantedAuthority(it) }
                    )
                }
        }
        return webFilter
    }

    private fun resolveToken(exchange: ServerWebExchange): Mono<String> {
        return Mono.justOrEmpty(exchange.request.headers.getFirst(HttpHeaders.AUTHORIZATION).takeIf {
            it?.contains(authTheme) ?: false
        })
    }

    private fun validateToken(token: String): Mono<Boolean> {
        return webClient.get()
            .uri("/isValid")
            .header(HttpHeaders.AUTHORIZATION, token)
            .accept(MediaType.APPLICATION_JSON).retrieve()
            .onStatus(HttpStatus::isError) { response ->
                response.bodyToMono<AuthErrorResponse>()
                    .handle { error, sink ->
                        sink.error(ResponseStatusException(HttpStatus.UNAUTHORIZED, error.message))
                    }
            }
            .bodyToMono(String::class.java)
            .thenReturn(true)
    }


    private fun customersAuthenticationManager(): ReactiveAuthenticationManager {
        return ReactiveAuthenticationManager { authentication ->
            // not used at now
            Mono.just(authentication)
        }
    }
}