package ru.kemsu.misc.service.security.model

data class AuthData(
        val id: Long,
        val username: String,
        val exp: Long,
        val roles: Set<String>,
        var token: String? = null
)
