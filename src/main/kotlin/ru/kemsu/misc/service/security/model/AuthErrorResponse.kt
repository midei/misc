package ru.kemsu.misc.service.security.model


data class AuthErrorResponse(val message: String)
