package ru.kemsu.misc.legobot.service

import com.google.api.services.calendar.model.Event
import java.io.IOException
import java.time.LocalDateTime

interface ICalendar {
    @Throws(IOException::class)
    fun freeBusy(calendars: Array<String>, start: LocalDateTime, end: LocalDateTime): Collection<String>
    fun insertEvent(calendarId: String, start: LocalDateTime, end: LocalDateTime, displayName: String, email: String, summary: String, description: String?): Event
    fun deleteEvent(calendarId: String, eventId: String)
    fun getEvent(calendarId: String, eventId: String): Event?
    fun listEvents(calendars: Array<String>, start: LocalDateTime, end: LocalDateTime): Collection<Event>
    fun findEvent(calendarId: String, start: LocalDateTime, attendeeEmail : String): Event
    fun addUserToRoom(calendarId: String, mail : String, role : String)
}