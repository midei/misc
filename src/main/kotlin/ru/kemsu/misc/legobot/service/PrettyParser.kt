package ru.kemsu.misc.legobot.service

import org.ocpsoft.prettytime.nlp.PrettyTimeParser
import org.springframework.stereotype.Component
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

@Component
class PrettyParser {

    private val userDatePattern = "(\\d{1,2}).([а-я]+|\\d{1,2}).?(\\d{1,4})?".toRegex()

    private val keywords = mapOf(
        "сейчас" to "now",
        "эт" to "this",
        "след" to "next",
        "сегодн" to "today",
        "завтр" to "tomorrow",
        "послезавтр" to "day after tomorrow",
        "понедельник" to "monday",
        "пн" to "monday",
        "вторник" to "tuesday",
        "вт" to "tuesday",
        "сред" to "wednesday",
        "ср" to "wednesday",
        "четверг" to "thursday",
        "чт" to "thursday",
        "пятниц" to "friday",
        "пт" to "friday",
        "суббот" to "saturday",
        "сб" to "saturday",
        "воскр" to "sunday",
        "вс" to "sunday",
        "утр" to "morning",
        "вечер" to "evening",
        "минут" to "minute",
        "час" to "hour",
        "ден" to "day",
        "недел" to "week",
        "месяц" to "month",
        "год" to "year",
        "январ" to "january",
        "янв" to "january",
        "феврал" to "february",
        "фев" to "february",
        "март" to "march",
        "апрел" to "april",
        "ма" to "may",
        "июн" to "june",
        "июл" to "july",
        "август" to "august",
        "авг" to "august",
        "сентябр" to "september",
        "сен" to "september",
        "октябр" to "october",
        "окт" to "october",
        "ноябр" to "november",
        "декабр" to "december",
        "дек" to "december"
    )
    private val keywordsPatterns = keywords.map { "(^|\\s)${it.key}[^ ]*".toRegex() to it.value }

    fun parse(date: String, time: String): Date? {
        val parsedUserData = parseUserData(date.lowercase())
        if (parsedUserData != null) return dateFromSimpleFormat("$parsedUserData $time")
        val formattedData = format(date.lowercase()) ?: return null
        val dates = PrettyTimeParser().parse("$formattedData $time")
        return if (dates.size == 1) dates[0] else null
    }

    private fun format(date: String): String? {
        val pattern = keywordsPatterns.firstOrNull { it.first.find(date) != null } ?: return null
        return " ${pattern.second} "
    }


    private fun parseUserData(textDate: String): String? {
        val parseResult = userDatePattern.find(textDate) ?: return null
        val (day, month, year) = parseResult.destructured
        return if (year.isEmpty()) "$day.$month.${LocalDate.now().year}"
        else "$day.$month.$year"
    }

    private fun dateFromSimpleFormat(dateAndTime: String): Date? {
        return parseByPattern("d.M.y HH:mm", dateAndTime)
            ?: parseByPattern("d.MMMM.y HH:mm", dateAndTime)
    }

    @Throws(ParseException::class)
    private fun parseByPattern(pattern: String, dateAndTime: String): Date? {
        return try {
            val simpleDateFormat = SimpleDateFormat(pattern, Locale("ru", "RU"))
            simpleDateFormat.parse(dateAndTime)
        } catch (e: ParseException) {
            null
        }
    }

}