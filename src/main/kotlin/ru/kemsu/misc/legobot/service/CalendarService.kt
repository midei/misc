package ru.kemsu.misc.legobot.service

import com.google.api.client.util.DateTime
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.model.*


import org.springframework.stereotype.Service
import ru.kemsu.misc.helper.printError
import ru.kemsu.misc.legobot.util.toDateTime
import java.io.IOException
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit


@Service
class CalendarService(private val calendar: Calendar) : ICalendar {

    @Throws(IOException::class)
    override fun freeBusy(calendars: Array<String>, start: LocalDateTime, end: LocalDateTime): Collection<String> {
        val request = FreeBusyRequest()
            .setTimeMin(start.toDateTime())
            .setTimeMax(end.toDateTime())
            .setItems(calendars.map { FreeBusyRequestItem().setId(it) })
        val response = calendar.freebusy().query(request).execute()
        return response.calendars.filter { it.value.busy.isEmpty() }.map { (id, _) -> id }
    }

    override fun insertEvent(calendarId: String, start: LocalDateTime, end: LocalDateTime, displayName: String, email: String, summary: String, description: String?): Event {
        val eventAttendee = EventAttendee().setDisplayName(displayName).setEmail(email)
        val event: Event = Event()
            .setSummary(summary)
            .setStart(EventDateTime().setDateTime(start.toDateTime()))
            .setEnd(EventDateTime().setDateTime(end.toDateTime()))
            .setAttendees(listOf(eventAttendee))
        description?.let { event.setDescription(it) }
        return calendar.events().insert(calendarId, event).execute();
    }


    override fun deleteEvent(calendarId: String, eventId: String) {
        calendar.events().delete(calendarId, eventId).execute()
    }

    override fun getEvent(calendarId: String, eventId: String): Event? = calendar.events().get(calendarId, eventId).execute()


    override fun listEvents(calendars: Array<String>, start: LocalDateTime, end: LocalDateTime): Collection<Event> {
        return calendars.map { calendarId ->
            calendar.events().list(calendarId)
                .setTimeMin(start.toDateTime())
                .setTimeMax(end.toDateTime())
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute().items
                .filter { it.attendees != null }
        }.flatten()
    }

    override fun findEvent(calendarId: String, start: LocalDateTime, attendeeEmail: String): Event {
        return calendar.events().list(calendarId).setTimeMin(start.toDateTime())
            .setOrderBy("startTime")
            .setSingleEvents(true)
            .execute().items.stream()
            .filter { it.attendees != null }
            .filter { it.attendees.any { attendee -> attendee.email == attendeeEmail } }
            .filter { compareDateTruncated(it.start.dateTime, start) }
            .findFirst().orElseThrow { Exception("Event not found") }
    }

    private fun compareDateTruncated(eventDataAndTime: DateTime, eventStartDataAndTime: LocalDateTime): Boolean {
        return ZonedDateTime.parse(eventDataAndTime.toString()).toLocalDateTime().truncatedTo(ChronoUnit.MINUTES) == eventStartDataAndTime
    }


    override fun addUserToRoom(calendarId: String, mail: String, role: String) {
        val rule = AclRule()
        val scope = AclRule.Scope()
        scope.type = "user"
        scope.value = mail

        rule.scope = scope
        rule.role = role

        try {
            calendar.acl().insert(calendarId, rule).execute()
        } catch (e: IOException) {
            "${e.message} for $calendarId".printError()
        }
    }
}