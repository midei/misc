package ru.kemsu.misc.legobot.service

import com.google.api.services.sheets.v4.model.AppendValuesResponse


import org.springframework.stereotype.Service
import ru.kemsu.misc.legobot.model.*
import ru.kemsu.misc.legobot.util.toLocalDateTime
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class LegobotService(
    private val calendarService: ICalendar,
    private val prettyParser: PrettyParser
) {

    @Throws(Exception::class)
    fun freeBusy(freeBusyData: FreeBusyData): Collection<String> {
        val parsedDate = parsedDate(freeBusyData.atDate, freeBusyData.atTime)
        val localDateTime = parsedDate.toLocalDateTime().checkLessThanNow()
        return calendarService.freeBusy(
            freeBusyData.calendars,
            localDateTime,
            localDateTime.plusSeconds(freeBusyData.durationInSeconds)
        )
    }

    @Throws(Exception::class)
    fun insertEvent(eventData: EventData): RegisteredEvent {
        val parsedDate = parsedDate(eventData.atDate, eventData.atTime)
        val startTime = parsedDate.toLocalDateTime().checkLessThanNow()
        val endTime = startTime.plusSeconds(eventData.durationInSeconds)
        checkRoomIsBusy(eventData.calendarId, startTime, endTime)
        val event = calendarService.insertEvent(
            eventData.calendarId, startTime, endTime,
            eventData.displayName, eventData.email, eventData.summary, eventData.description
        )
        return RegisteredEvent(event.id, event.htmlLink, startTime.toString())
    }

    @Throws(Exception::class)
    private fun checkRoomIsBusy(calendarId: String, startTime: LocalDateTime, endTime: LocalDateTime) {
        val isBusy = calendarService.freeBusy(arrayOf(calendarId), startTime, endTime).isEmpty()
        if (isBusy) throw Exception("Переговорка уже занята")
    }

    private fun LocalDateTime.checkLessThanNow(): LocalDateTime {
        return if (this < LocalDateTime.now().minusMinutes(5)) throw Exception("Дата не может быть меньше текущей")
        else this
    }

    @Throws(Exception::class)
    private fun parsedDate(atDate: String, atTime: String): Date =
        prettyParser.parse(atDate, atTime) ?: throw Exception("Не удалось определить дату")

    @Throws(Exception::class)
    fun parsedDate(atDate: String): String {
        val parsedDateTime = parsedDate(atDate, LocalTime.now().toString()).toLocalDateTime()
        return parsedDateTime.truncatedTo(ChronoUnit.DAYS).toString()
    }


    fun deleteEventForUser(deleteEventForUserData: DeleteEventForUserData) {
        val parsedDate = parsedDate(deleteEventForUserData.atDate, deleteEventForUserData.atTime)
        val localDateTime = parsedDate.toLocalDateTime()
        val event = calendarService.findEvent(
            deleteEventForUserData.calendarId,
            localDateTime,
            deleteEventForUserData.attendeeEmail.lowercase()
        )
        calendarService.deleteEvent(deleteEventForUserData.calendarId, event.id)
    }

    fun listEventsForUser(calendars: Array<String>, attendeeEmail: String, days: Long): List<ResponseEventForUser> {
        val now = LocalDateTime.now()
        val attendeeEmailLowerCase = attendeeEmail.lowercase()
        return calendarService.listEvents(calendars, now, now.plusDays(days))
            .filter { it.attendees.any { attendee -> attendee.email == attendeeEmailLowerCase } }
            .map {
                ResponseEventForUser(
                    it.organizer.email,
                    it.summary,
                    it.start.dateTime.toString()
                )
            }.sortedBy { it.startDate }
    }
}