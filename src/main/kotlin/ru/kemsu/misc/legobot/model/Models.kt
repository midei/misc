package ru.kemsu.misc.legobot.model

data class FreeBusyData(
    val calendars: Array<String>,
    val atDate: String,
    val atTime: String = "",
    val durationInSeconds: Long,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FreeBusyData

        if (!calendars.contentEquals(other.calendars)) return false
        if (atDate != other.atDate) return false
        if (atTime != other.atTime) return false
        if (durationInSeconds != other.durationInSeconds) return false

        return true
    }

    override fun hashCode(): Int {
        var result = calendars.contentHashCode()
        result = 31 * result + atDate.hashCode()
        result = 31 * result + atTime.hashCode()
        result = 31 * result + durationInSeconds.hashCode()
        return result
    }


}

data class EventsForUserData(
    val calendars: Array<String>,
    val attendeeEmail: String,
    val days: Long,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EventsForUserData

        if (!calendars.contentEquals(other.calendars)) return false
        if (attendeeEmail != other.attendeeEmail) return false

        return true
    }

    override fun hashCode(): Int {
        var result = calendars.contentHashCode()
        result = 31 * result + attendeeEmail.hashCode()
        return result
    }
}

data class EventData(
    val calendarId: String,
    val atDate: String,
    val atTime: String = "",
    val durationInSeconds: Long,
    val displayName: String,
    val email: String,
    val summary: String,
    val description: String?,
)

data class RegisteredEvent(
    val eventId: String,
    val htmlLink: String,
    val startDateAndTime: String,
)

data class ResponseEventForUser(
    val calendarId: String,
    val summary: String,
    val startDate: String,
)

data class DeleteEventForUserData(
    val calendarId: String,
    val atDate: String,
    val atTime: String,
    val attendeeEmail: String,
)

data class AppendedData(
    val valueInputOption: String = "USER_ENTERED",
    val spreadsheetId: String,
    val range: String,
    val values: List<String>
)

