package ru.kemsu.misc.legobot.rest

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import ru.kemsu.misc.helper.getGoogleApiMessage
import ru.kemsu.misc.legobot.model.*
import ru.kemsu.misc.legobot.service.ICalendar
import ru.kemsu.misc.legobot.service.LegobotService

@RestController
@RequestMapping("/misc/api/v1/calendar")
class CalendarResource(private val calendarService: ICalendar, private val legobotService: LegobotService) {
    private val logger = LoggerFactory.getLogger(CalendarResource::class.java)

    @GetMapping(path = ["/freeBusy"])
    suspend fun freeBusy(@RequestBody freeBusyData: FreeBusyData): Collection<String> = withContext(Dispatchers.IO) {
        runCatching {
            legobotService.freeBusy(freeBusyData)
        }.getOrElse {
            val message = it.getGoogleApiMessage()
            logger.error("calendars with params: $freeBusyData : $message ")
            throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED, message)
        }
    }

    @PostMapping(path = ["/insertEvent"])
    suspend fun insertEvent(@RequestBody eventData: EventData): RegisteredEvent = withContext(Dispatchers.IO) {
        runCatching {
            legobotService.insertEvent(eventData)
        }.getOrElse {
            val message = it.getGoogleApiMessage()
            logger.error("insertEvent with params: $eventData : $message")
            throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED, message)
        }
    }

    @DeleteMapping(path = ["/deleteEvent"])
    suspend fun deleteEvent(
        @RequestParam("calendarId") calendarId: String,
        @RequestParam("eventId") eventId: String,
    ): Unit = withContext(Dispatchers.IO) {
        runCatching {
            calendarService.deleteEvent(calendarId, eventId)
        }.getOrElse {
            val message = it.getGoogleApiMessage()
            logger.error("deleteEvent with params: $calendarId, $eventId : $message ")
            throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED, message)
        }
    }

    @GetMapping(path = ["/listEventsForUser"])
    suspend fun listEventsForUser(@RequestBody eventsForUserData: EventsForUserData): Collection<ResponseEventForUser> = withContext(Dispatchers.IO) {
        runCatching {
            legobotService.listEventsForUser(eventsForUserData.calendars, eventsForUserData.attendeeEmail, eventsForUserData.days)
        }.getOrElse {
            val message = it.getGoogleApiMessage()
            logger.error(" listEventsForUser with params: $eventsForUserData : $message ")
            throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED, message)
        }
    }


    @DeleteMapping(path = ["/deleteEventForUser"])
    suspend fun deleteEventForUser(@RequestBody deleteEventForUserData: DeleteEventForUserData): Unit = withContext(Dispatchers.IO) {
        runCatching {
            legobotService.deleteEventForUser(deleteEventForUserData)
        }.getOrElse {
            val message = it.getGoogleApiMessage()
            logger.error("deleteEvent with params: $deleteEventForUserData : $message")
            throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED, message)
        }
    }

    @GetMapping(path = ["/parsedDate"], produces = [MediaType.APPLICATION_JSON_VALUE])
    suspend fun parsedDate(@RequestParam("atDate") atDate: String): String = withContext(Dispatchers.IO) {
            legobotService.parsedDate(atDate)
    }

}