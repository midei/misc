package ru.kemsu.misc.legobot.util

import com.google.api.client.util.DateTime
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

fun LocalDateTime.toDateTime(): DateTime = DateTime(Date.from(this.atZone(ZoneId.systemDefault()).toInstant()))
fun Date.toLocalDateTime(): LocalDateTime = Instant.ofEpochMilli(this.time)
    .atZone(ZoneId.systemDefault())
    .toLocalDateTime()
