package ru.kemsu.misc.config


import org.springframework.boot.web.error.ErrorAttributeOptions
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import ru.kemsu.misc.helper.printError

@Component
class GlobalErrorAttributes : DefaultErrorAttributes() {
    override fun getErrorAttributes(request: ServerRequest, options: ErrorAttributeOptions): Map<String, Any> {
        val errorAttributes = super.getErrorAttributes(request, options.including(ErrorAttributeOptions.Include.MESSAGE))
        request.bodyToMono(String::class.java).defaultIfEmpty("").subscribe { body ->
            "method=${request.method()} path=${request.path()} params=[${request.queryParams()}] body=[$body] message=[${errorAttributes["message"]}]".printError()
        }
        errorAttributes["status"] = HttpStatus.EXPECTATION_FAILED.value()
        errorAttributes["error"] = HttpStatus.EXPECTATION_FAILED.reasonPhrase
        return errorAttributes
    }
}