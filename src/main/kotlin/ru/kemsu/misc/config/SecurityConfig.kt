package ru.kemsu.misc.config


import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.HttpStatusServerEntryPoint
import ru.kemsu.misc.service.security.AuthService

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class SecurityConfig(private val authService: AuthService) {

    @Bean
    fun securityWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http
            .authorizeExchange()
            .anyExchange().authenticated()
            .and()
            .addFilterAt(authService.authenticationWebFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
            .csrf().disable()
            .httpBasic().authenticationEntryPoint(HttpStatusServerEntryPoint(HttpStatus.UNAUTHORIZED)).disable()
            .formLogin().disable()
            .build()
    }
}

