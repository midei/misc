package ru.kemsu.misc.config

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.SheetsScopes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File

@Configuration
class GoogleAppApi {
    private val jsonFactory: JsonFactory = JacksonFactory.getDefaultInstance()
    private val credentials = "credentials.json"
    private val store = "tokens"

    @Bean
    fun createCalendar(): Calendar {
        val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
        return Calendar.Builder(httpTransport, jsonFactory, getCredentials(httpTransport))
            .setApplicationName("BOT")
            .build()
    }

    private fun getCredentials(httpTransport: NetHttpTransport): Credential {
        val clientSecrets = GoogleClientSecrets.load(jsonFactory, File(credentials).reader())
        val flow = GoogleAuthorizationCodeFlow.Builder(
            httpTransport,
            jsonFactory,
            clientSecrets, listOf(CalendarScopes.CALENDAR)
        ).setDataStoreFactory(FileDataStoreFactory(File(store))).setAccessType("offline").build()
        return flow.loadCredential("user") ?: throw Exception("Credential not found")
    }
}