import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
val prettytimeVersion = "5.0.6.Final"
plugins {
	id("org.springframework.boot") version "2.7.3"
	id("io.spring.dependency-management") version "1.0.13.RELEASE"
	kotlin("jvm") version "1.6.21"
	kotlin("plugin.spring") version "1.6.21"
}

group = "ru.kemsu"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
	// https://mvnrepository.com/artifact/com.google.api-client/google-api-client
	implementation("com.google.api-client:google-api-client:1.31.1")
	// https://mvnrepository.com/artifact/com.google.apis/google-api-services-sheets
	implementation("com.google.apis:google-api-services-sheets:v4-rev20221216-2.0.0")
    // https://mvnrepository.com/artifact/com.google.apis/google-api-services-calendar
	implementation("com.google.apis:google-api-services-calendar:v3-rev194-1.22.0")
	// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-webflux
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-security
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	// https://mvnrepository.com/artifact/org.ocpsoft.prettytime/prettytime
	implementation("org.ocpsoft.prettytime:prettytime:$prettytimeVersion")
	// https://mvnrepository.com/artifact/org.ocpsoft.prettytime/prettytime-nlp
	implementation("org.ocpsoft.prettytime:prettytime-nlp:$prettytimeVersion")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	description = "Runs the integration tests"
	group = "verification"
	useJUnitPlatform()
}



